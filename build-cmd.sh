#!/bin/bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(dirname "$0")"

PROJECT=gtk
LICENSE=README
VERSION="2.24.28"
SOURCE_DIR="$PROJECT+-$VERSION"


if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

stage="$(pwd)"
GLIB_INCLUDE="${stage}"/packages/include/glib
CAIRO_INCLUDE="${stage}"/packages/include/cairo
HARFBUZZ_INCLUDE="${stage}"/packages/include/harfbuzz
GDKPIXBUF_INCLUDE="${stage}"/packages/include/gdk-pixbuf
PANGO_INCLUDE="${stage}"/packages/include/pango
ATK_INCLUDE="${stage}"/packages/include/atk

[ -f "$GLIB_INCLUDE"/glib.h ] || fail "You haven't installed the glib package yet."
[ -f "$HARFBUZZ_INCLUDE"/hb.h ] || fail "You haven't installed the harfbuzz package yet."
[ -f "$CAIRO_INCLUDE"/cairo.h ] || fail "You haven't installed the cairo package yet."
[ -f "$GDKPIXBUF_INCLUDE"/gdk-pixbuf.h ] || fail "You haven't installed the gdk-pixbuf package yet."
[ -f "$PANGO_INCLUDE"/pango.h ] || fail "You haven't installed the pango package yet."
[ -f "$ATK_INCLUDE"/atk.h ] || fail "You haven't installed the atk package yet."

case "$AUTOBUILD_PLATFORM" in
    "linux")
        # Prefer gcc-4.9 if available.
        if [[ -x /usr/bin/gcc-4.9 && -x /usr/bin/g++-4.9 ]]; then
            export CC=/usr/bin/gcc-4.9
            export CXX=/usr/bin/g++-4.9
        fi

        # Default target to 32-bit
        opts="${TARGET_OPTS:--m32}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        pushd "$TOP/$SOURCE_DIR"
		   CPPFLAGS="-I${stage}/#packages/include $TARGET_CPPFLAGS"
#           LDFLAGS="-L${stage}/packages/lib/release"
#           PKG_CONFIG_PATH=
#           export CPPFLAGS LDFLAGS LD_LIBRARY_PATH
#           LD_LIBRARY_PATH="${stage}/packages/lib/release"
#           PATH="${stage}/packages/include:${stage}/packages/lib/release:$PATH"
#           export LD_LIBRARY_PATH PATH
            CFLAGS="-I${stage}/packages/include -I$ATK_INCLUDE -I$CAIRO_INCLUDE -I$PANGO_INCLUDE -I$HARFBUZZ_INCLUDE -I$GDKPIXBUF_INCLUDE $opts -O3 -fPIC -DPIC"\
            ./configure --prefix="$stage" 
            make
            make install
		    mkdir -p "$stage/include/gtk"
		    cp -a gtk/*.h "$stage/include/gtk"
		    mkdir -p "$stage/include/gdk"
		    cp -a gdk/*.h "$stage/include/gdk"

        popd

        mv lib release
        mkdir -p lib
        mv release lib

    ;;
    "linux64")
        # Prefer gcc-4.6 if available.
        if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
            export CC=/usr/bin/gcc-4.6
            export CXX=/usr/bin/g++-4.6
        fi

        # Default target to 64-bit
        opts="${TARGET_OPTS:--m64}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        pushd "$TOP/$SOURCE_DIR"
		   CPPFLAGS="-I${stage}/#packages/include $TARGET_CPPFLAGS"
#           LDFLAGS="-L${stage}/packages/lib/release"
#           PKG_CONFIG_PATH=
#           export CPPFLAGS LDFLAGS LD_LIBRARY_PATH
#           LD_LIBRARY_PATH="${stage}/packages/lib/release"
#           PATH="${stage}/packages/include:${stage}/packages/lib/release:$PATH"
#           export LD_LIBRARY_PATH PATH
            CFLAGS="-I${stage}/packages/include -I$ATK_INCLUDE -I$CAIRO_INCLUDE -I$PANGO_INCLUDE -I$HARFBUZZ_INCLUDE -I$GDKPIXBUF_INCLUDE $opts -O3 -fPIC -DPIC"\
            ./configure --prefix="$stage" 
            make
            make install
		    mkdir -p "$stage/include/gtk"
		    cp -a gtk/*.h "$stage/include/gtk"
		    mkdir -p "$stage/include/gdk"
		    cp -a gdk/*.h "$stage/include/gdk"

        popd

        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    *)
        echo "platform not supported"
        exit -1
    ;;
esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$SOURCE_DIR/$LICENSE" "$stage/LICENSES/$PROJECT.txt"
echo "$VERSION" > "$stage/VERSION.txt"

pass

